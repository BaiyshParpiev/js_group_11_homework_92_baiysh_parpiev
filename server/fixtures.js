const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  await User.create({
    email: 'bayish.parpiev@gmail.com',
    password: 'bayish',
    displayName: 'Bayish',
    friends: [],
    token: nanoid(),
    status: 'offline',
  }, {
    email: 'sezim.duishebekova@gmail.com',
    password: 'sezim',
    displayName: 'Sezim',
    token: nanoid(),
    status: 'offline',
    friends: [],
  });

  await mongoose.connection.close();
};

run().catch(console.error);