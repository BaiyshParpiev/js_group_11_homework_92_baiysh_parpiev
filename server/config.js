const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  db: {
    url: 'mongodb://localhost/chat',
  },
  google: {
    clientId: '7863750959-hr6t5l1kpqng1o55tifoctmr45p0529d.apps.googleusercontent.com'
  },
};