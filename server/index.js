const express = require('express');
const config = require('./config');
const cors = require('cors');
const exitHook = require('async-exit-hook');
const mongoose = require("mongoose");
const users = require('./app/users');
const app = express();
const User = require('./models/User');

require('express-ws')(app);

app.use(express.json());
app.use(cors());
app.use(express.static('public'));


app.use('/users', users);
const port = 8000;

const activeConnection = {};
const messages = [];
const us = [];
const run = async () => {
    await mongoose.connect(config.db.url);

    app.ws('/chat', async (ws, req) => {
        const id = req.query.token;
        const user = await User.findById(id);
        us.push(user);

        activeConnection[id] = ws;

        Object.keys(activeConnection).forEach(key => {
            const connection = activeConnection[key];

            connection.send(JSON.stringify({
                type: "CHANGES",
                payload: {
                    users: us,
                },
            }))
        });


        ws.send(JSON.stringify({
            type: 'CONNECTED',
            payload: messages.length > 30 ? messages.slice(-30) : messages,
        }));

        ws.on('message', msg => {
            const parsedMessage = JSON.parse(msg);

            switch (parsedMessage.type) {
                case "CREATE_NEW_MESSAGE":
                    Object.keys(activeConnection).forEach(key => {
                        const connection = activeConnection[key];

                        connection.send(JSON.stringify({
                            type: "NEW_MESSAGE",
                            payload: {
                                message: parsedMessage.message,
                                author: parsedMessage.author,
                                datetime: new Date(Date.UTC(2012, 11, 20, 3, 0, 0)).toLocaleDateString('en-GB'),
                            },
                        }))
                    });

                    messages.push({
                        message: parsedMessage.message,
                        author: parsedMessage.author,
                        datetime: new Date(Date.UTC(2012, 11, 20, 3, 0, 0)).toLocaleDateString('en-GB'),
                    });
                    break;
                default:
                    console.log('Unknown type.', parsedMessage.type);
            }
        });

        ws.on('close', async() => {
            console.log(`Clit disconnected with id=${id}`);
            for (let i = 0; i < us.length; i++){
                if(us[i]._id === id){
                    us.splice(i, 1);
                }
            };
            Object.keys(activeConnection).forEach(key => {
                const connection = activeConnection[key];

                connection.send(JSON.stringify({
                    type: "CHANGES",
                    payload: {
                        users: us,
                    },
                }))
            });
            delete activeConnection[id]
        });
    });

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(() => {
        console.log('exiting');
        mongoose.disconnect();
    });
};

run().catch(e => console.error(e));