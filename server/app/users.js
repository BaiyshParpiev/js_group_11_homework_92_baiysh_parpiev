const express = require('express');
const User = require('../models/User');
const auth = require("../middleware/auth");
const config = require('../config');
const {OAuth2Client} = require('google-auth-library')
const {nanoid} = require("nanoid");
const client = new OAuth2Client(config.google.clientId);

const router = express.Router();

router.get('/', auth, async(req, res) => {
  try{
    const data = await User.find();
    res.send(data);
  }catch{
    res.status(403).send({message: 'Server error!'})
  }
})

router.post('/', async (req, res) => {
  try {
    const user = new User({
      email: req.body.email,
      displayName: req.body.displayName,
      password: req.body.password,
    });

    user.generateToken();
    await user.save();
    res.send(user);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({email: req.body.email});

  if (!user) {
    return res.status(401).send({message: 'Credentials are wrong!'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({message: 'Credentials are wrong!'});
  }

  user.generateToken();
  await user.save({validateBeforeSave: false});

  res.send({message: 'Email and password correct!', user});
});

router.post('/googleLogin', async (req, res) => {
  try {
    const ticket = await client.verifyIdToken({
      idToken: req.body.tokenId,
      audience: config.google.clientId,
    });
    const {name, email, sub: ticketUserId} = ticket.getPayload();

    if(req.body.googleId !== ticketUserId){
      return res.status(401).send({global: 'User ID incorrect!'})
    }
    let user = await User.findOne({email});
    if (!user) {
      user = new User({
        email,
        password: nanoid(),
        displayName: name
      })
    }

    user.generateToken();
    await user.save({validateBeforeSave: false});
    res.send({message: 'Success', user})
  } catch (e) {
      res.status(500).send({global: 'Server error, please try again'});
  }
})

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Success'};

  if (!token) return res.send(success);

  const user = await User.findOne({token});

  if (!user) return res.send(success);

  user.generateToken();

  await user.save({validateBeforeSave: false});

  return res.send(success);
});

router.post('/add/:id', auth, async(req, res)=> {
    try{
        const {id} = req.params;
        const friend = await User.findById(id);
        if(!friend){
          return res.status(401).send('Data not valid');
        }

        req.user.friends.push(friend)
        await User.findByIdAndUpdate(req.params.id, req.user, {new: true});
        console.log(req.user)
    }catch{
      res.sendStatus(500);
    }
})


module.exports = router;





















