import React, {useEffect, useRef, useState} from 'react';
import UsersLayout from "../../components/UI/Layout/UsersLayout";
import {Box, Button, Container, Grid, makeStyles, Paper, TextField, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchUsers} from "../../store/actions/usersActions";

const useStyles = makeStyles(theme => ({
    div: {
        width: '100%',
        flexDirection: 'column',
    },
    children: {
        width: '50%'
    },
    paper: {
        margin: '20px 0',
        width: '100%',
        backgroundColor: 'skyblue',
    }
}))

const All = () => {
    const classes = useStyles();
    const [message, setMessage] = useState('');
    const {messages} = useSelector(state => state.messages);
    const {user} = useSelector(state => state.users);
    const dispatch = useDispatch();

    const ws = useRef(null);

    const start = (websocketServerLocation) => {
        ws.current = new WebSocket(websocketServerLocation);
    }

    let interval;
    ws.current.onclose = () => {
         interval = setInterval(() => {start("ws://localhost:8000/chat?token=" + user._id)}, 10000)
    };

    ws.current.on = () => {
        clearInterval(interval);
    }

    useEffect(() => {
        ws.current = new WebSocket("ws://localhost:8000/chat?token=" + user._id);
        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);
            dispatch(decoded)
        };
        dispatch(fetchUsers());

    }, [dispatch, user._id]);



    const inputChangeHandler = e => {
        setMessage(e.target.value);
    };

    const sendMessage = e => {
        e.preventDefault();
        ws.current.send(JSON.stringify({
            type: "CREATE_NEW_MESSAGE",
            author: user,
            message,
        }));
        setMessage('');
    }

    return (
        <UsersLayout>
            <Container maxWidth="xl">
                <Grid container className={classes.div}>
                    {messages && messages.map((m, i) => {
                        return user === m.author ? (
                            <Grid key={i} container item alignself="flex-end" className={classes.children}>
                                <Paper component={Box} p={2} className={classes.paper}>
                                    <Typography variant="subtitle2">{m.datetime}</Typography>
                                    <Typography variant="body1">{m.author.displayName}</Typography>
                                    <Typography variant="h4">{m.message}</Typography>
                                </Paper>
                            </Grid>
                        ) : (
                            <Grid key={i} container item alignself="flex-start" className={classes.children}>
                                <Paper component={Box} p={2} className={classes.paper}>
                                    <Typography variant="subtitle2">{m.datetime}</Typography>
                                    <Typography variant="body1">{m.author.displayName}</Typography>
                                    <Typography variant="h4">{m.message}</Typography>
                                </Paper>
                            </Grid>
                        )
                    })}
                </Grid>
                <Grid component="form" flexdirection="row" container justifyContent="flex-start">
                    <Grid item md>
                        <TextField
                            fullWidth
                            type="text"
                            label="Your message"
                            onChange={inputChangeHandler}
                            name="message"
                            value={message}
                            multiline
                            variant="outlined"
                        />
                    </Grid>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        onClick={sendMessage}
                    >
                        Send
                    </Button>
                </Grid>
            </Container>
        </UsersLayout>
    );
};

export default All;