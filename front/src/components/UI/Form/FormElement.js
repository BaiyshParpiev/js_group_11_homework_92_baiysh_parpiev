import React from 'react';
import PropTypes from 'prop-types';
import {Grid, MenuItem, TextField, IconButton, InputAdornment,} from "@material-ui/core";
import {Visibility, VisibilityOff} from "@material-ui/icons";

const FormElement = ({handleShowPassword, label, name, value, onChange, required, error, autoComplete, type, select, options, multiline, rows}) => {
    let inputChildren = null;

    if (select) {
        inputChildren = options.map(option => (
            <MenuItem
                key={option._id}
                value={option._id}
            >
                {option.title}
            </MenuItem>
        ));
    }

    return (
        <Grid item xs={12}>
            <TextField
                fullWidth
                variant="outlined"
                select={select}
                multiline={multiline}
                rows={rows}
                type={type}
                required={required}
                autoComplete={autoComplete}
                label={label}
                name={name}
                value={value}
                onChange={onChange}
                error={Boolean(error)}
                helperText={error}
                InputProps={name === 'password' ? {
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton onClick={handleShowPassword}>
                                {type === "password" ? <Visibility/> : <VisibilityOff/>}
                            </IconButton>
                        </InputAdornment>
                    )
                } : null}
            >
                {inputChildren}
            </TextField>
        </Grid>
    );
};

FormElement.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    required: PropTypes.bool,
    error: PropTypes.string,
    autoComplete: PropTypes.string,
    type: PropTypes.string,
    select: PropTypes.bool,
    options: PropTypes.arrayOf(PropTypes.object),
    multiline: PropTypes.bool,
    rows: PropTypes.number
};

export default FormElement;