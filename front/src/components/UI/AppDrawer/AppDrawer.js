import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles, useTheme} from '@material-ui/core/styles';
import {CssBaseline, Divider, Drawer, Hidden, MenuItem, MenuList} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {fetchUsers} from "../../../store/actions/usersActions";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
        position: "absolute",
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
}));

const AppDrawer = () => {
    const classes = useStyles();
    const theme = useTheme();
    const dispatch = useDispatch();
    const [mobileOpen, setMobileOpen] = useState(false);
    const {users} = useSelector(state => state.messages.users)


    useEffect(() => {
        dispatch(fetchUsers());
    }, [dispatch]);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const handleDrawerClose = () => {
        setMobileOpen(false);
    };



    const drawer = (
        <div>
            <div className={classes.toolbar} />
            <Divider />
            <MenuList>
                <MenuItem
                    component={Link}
                    to="/"
                    onClick={handleDrawerClose}
                >
                    All
                </MenuItem>
                {users && users.map((u, i) => (
                    <MenuItem
                        key={i}
                        component={Link}
                        to={`/?user=${u && u._id}`}
                        onClick={handleDrawerClose}
                        style={{backgroundColor: 'green'}}
                    >
                        {u && u.displayName}
                    </MenuItem>
                ))}
            </MenuList>
            <Divider />
        </div>
    );

    return (
        <>
            <CssBaseline />
            <IconButton
                color="inherit"
                aria-label="open drawer"
                edge="start"
                onClick={handleDrawerToggle}
                className={classes.menuButton}
            >
                <MenuIcon />
            </IconButton>
            <nav className={classes.drawer}  aria-label="mailbox folders">
                <Hidden smUp implementation="css">
                    <Drawer
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true,
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
        </>
    );
};

export default AppDrawer;
