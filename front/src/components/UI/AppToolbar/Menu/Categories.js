import React, {useEffect, useState} from 'react';
import {Button, Menu, MenuItem} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories} from "../../../../store/actions/categoriesActions";
import {Link} from "react-router-dom";

const Categories = () => {
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);
    const {categories} = useSelector(state => state.categories)

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);


    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
                Categories
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {categories.map(category => (
                    <MenuItem
                        key={category._id}
                        component={Link}
                        to={`?category=${category._id}`}
                    >{category.title}</MenuItem>
                ))}
            </Menu>
        </>
    );
};

export default Categories;