import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";
import {toast} from "react-toastify";

export const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const FETCH_USER_REQUEST = 'FETCH_USER_REQUEST';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';

export const ADD_USER_REQUEST = 'ADD_USER_REQUEST';
export const ADD_USER_SUCCESS = 'ADD_USER_SUCCESS';
export const ADD_USER_FAILURE = 'ADD_USER_FAILURE';

export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGOUT_USER = 'LOGOUT_USER';

export const CLEAR_ERROR_USER = 'CLEAR_ERROR_USER';


export const registerUserRequest = () => ({type: REGISTER_USER_REQUEST});
export const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, payload: user});
export const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, payload: error});

export const fetchUserRequest = () => ({type: FETCH_USER_REQUEST});
export const fetchUserSuccess = users => ({type: FETCH_USER_SUCCESS, payload: users});
export const fetchUserFailure = error => ({type: FETCH_USER_FAILURE, payload: error});

export const addUserRequest = () => ({type: ADD_USER_REQUEST});
export const addUserSuccess = users => ({type: ADD_USER_SUCCESS, payload: users});
export const addUserFailure = error => ({type: ADD_USER_FAILURE, payload: error});

export const loginUserRequest = () => ({type: LOGIN_USER_REQUEST});
export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, payload: user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, payload: error});

export const clearErrorUser = () => ({type: CLEAR_ERROR_USER});

export const registerUser = userData => {
    return async dispatch => {
        try {
            dispatch(registerUserRequest());
            const response = await axiosApi.post('/users', userData);
            dispatch(registerUserSuccess(response.data));
            dispatch(historyPush('/'));
            toast.success('Sign up successful');
        } catch (error) {
            dispatch(registerUserFailure(error.response.data));
            toast.error(error.response.data.global);
        }
    };
};

export const loginUser = userData => {
    return async dispatch => {
        try {
            dispatch(loginUserRequest());
            const response = await axiosApi.post('/users/sessions', userData);
            dispatch(loginUserSuccess(response.data.user));
            dispatch(historyPush('/'));
            toast.success('Login successful');
        } catch (error) {
            dispatch(loginUserFailure(error.response.data));
            toast.error(error.response.data.global);
        }
    };
};

export const googleLogin = googleData => {
    return async dispatch => {
        try{
            const response = await axiosApi.post('/users/googleLogin', {
                tokenId: googleData.tokenId,
                googleId: googleData.googleId,
            });
            dispatch(loginUserSuccess(response.data.user));
            dispatch(historyPush('/'));
            toast.success('Login successful')
        }catch(error){
            dispatch(loginUserFailure(error.response.data));
            toast.error(error.response.data.global);
        }
    }
}

export const logoutUser = () => {
    return async (dispatch) => {
        await axiosApi.delete('/users/sessions');
        dispatch({type: LOGOUT_USER});
        dispatch(historyPush('/'));
    };
};

export const fetchUsers = () => async dispatch => {
    try{
        dispatch(fetchUserRequest());
        const {data} = await axiosApi.get('/users');
        dispatch(fetchUserSuccess(data));
    }catch(e){
        dispatch(fetchUserFailure(e));
        toast.success(e.response.data.message);
    }
};

export const addUser = id => async dispatch=> {
    try{
        dispatch(addUserRequest());
        const {data} = await axiosApi.post('/users/add/' + id);
        dispatch(addUserSuccess(data))
    }catch(e){
        dispatch(addUserFailure(e.response.data))
        toast.error(e.response.data.message)
    }
};
