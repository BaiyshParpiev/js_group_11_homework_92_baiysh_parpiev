const initialState = {
    messages: [],
    users: [],
};

const messagesReducer = (state = initialState, action) => {
    switch(action.type){
        case "CONNECTED":
            return {...state, messages: action.payload};
        case "NEW_MESSAGE":
            return {...state, messages: [...state.messages, action.payload]};
        case "CHANGES":
            return {...state, users: action.payload}
        default:
            return state;
    }
};

export default messagesReducer;
