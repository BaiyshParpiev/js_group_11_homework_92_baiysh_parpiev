import {
    ADD_USER_FAILURE,
    ADD_USER_REQUEST, ADD_USER_SUCCESS,
    CLEAR_ERROR_USER, FETCH_USER_FAILURE, FETCH_USER_REQUEST, FETCH_USER_SUCCESS,
    LOGIN_USER_FAILURE, LOGIN_USER_REQUEST,
    LOGIN_USER_SUCCESS, LOGOUT_USER,
    REGISTER_USER_FAILURE, REGISTER_USER_REQUEST,
    REGISTER_USER_SUCCESS
} from "../actions/usersActions";

export const initialState = {
    fetchError: null,
    users: [],
    registerLoading: false,
    registerError: null,
    addLoading: false,
    addError: null,
    loginError: null,
    loginLoading: false,
    user: null,
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case CLEAR_ERROR_USER:
            return {...state, registerError: null, loginError: null, fetchError: null};
        case ADD_USER_REQUEST:
            return {...state, addLoading: true}
        case ADD_USER_SUCCESS:
            return {...state, user: action.payload, addError: null, addLoading: false};
        case ADD_USER_FAILURE:
            return {...state, addError: action.payload, addLoading: false};
        case REGISTER_USER_REQUEST:
            return {...state, registerLoading: true}
        case REGISTER_USER_SUCCESS:
            return {...state, user: action.payload, registerError: null, registerLoading: false};
        case REGISTER_USER_FAILURE:
            return {...state, registerError: action.payload, registerLoading: false};
        case FETCH_USER_REQUEST:
            return {...state, fetchError: null}
        case FETCH_USER_SUCCESS:
            return {...state, users: action.payload, fetchError: null};
        case FETCH_USER_FAILURE:
            return {...state, fetchError: action.payload};
        case LOGIN_USER_REQUEST:
            return {...state, loginLoading: true}
        case LOGIN_USER_SUCCESS:
            return {...state, loginError: null, loginLoading: false, user: action.payload};
        case LOGIN_USER_FAILURE:
            return {...state, loginError: action.payload, loginLoading: false};
        case LOGOUT_USER:
            return {...state, user: null};
        default:
            return state;
    }
};

export default usersReducer;